-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2017 at 06:32 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_uasvisual2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_aset`
--

CREATE TABLE `tb_travel_m_aset` (
  `kode_aset` varchar(5) NOT NULL,
  `kode_kateg` varchar(5) NOT NULL,
  `nama_aset` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_jabkaryawan`
--

CREATE TABLE `tb_travel_m_jabkaryawan` (
  `kode_jabatan` varchar(2) NOT NULL,
  `nama_jabatan` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_jml_bangku_ka`
--

CREATE TABLE `tb_travel_m_jml_bangku_ka` (
  `kode_bangku` varchar(5) NOT NULL,
  `nama_bangku` varchar(5) NOT NULL,
  `jml_bangku` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_jml_bangku_kl`
--

CREATE TABLE `tb_travel_m_jml_bangku_kl` (
  `kode_bangku_kl` varchar(5) NOT NULL,
  `nama_bangku_kl` varchar(15) NOT NULL,
  `jml_bangku_kl` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_jml_bangku_pesawat`
--

CREATE TABLE `tb_travel_m_jml_bangku_pesawat` (
  `kode_bangku_pesawat` varchar(5) NOT NULL,
  `nama_bangku_pesawat` varchar(15) NOT NULL,
  `jml_bangku_pesawat` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_kapal_laut`
--

CREATE TABLE `tb_travel_m_kapal_laut` (
  `kode_kl` varchar(5) NOT NULL,
  `nama_kl` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_karyawan`
--

CREATE TABLE `tb_travel_m_karyawan` (
  `NIP` varchar(7) NOT NULL,
  `kode_jabatan` varchar(2) NOT NULL,
  `kode_posisi` varchar(2) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `password` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_kategpengajuan`
--

CREATE TABLE `tb_travel_m_kategpengajuan` (
  `kode_kateg` varchar(5) NOT NULL,
  `nama_kateg` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_keberangkatan_ka`
--

CREATE TABLE `tb_travel_m_keberangkatan_ka` (
  `kode_keberangkatan` varchar(5) NOT NULL,
  `kode_asal` varchar(5) NOT NULL,
  `kode_tujuan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_keberangkatan_kl`
--

CREATE TABLE `tb_travel_m_keberangkatan_kl` (
  `kode_keberangkatan_kl` varchar(5) NOT NULL,
  `kode_asal` varchar(5) NOT NULL,
  `kode_tujuan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_keberangkatan_pesawat`
--

CREATE TABLE `tb_travel_m_keberangkatan_pesawat` (
  `kode_keberangkatan_pesawat` varchar(5) NOT NULL,
  `kode_asal` varchar(5) NOT NULL,
  `kode_tujuan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_kereta`
--

CREATE TABLE `tb_travel_m_kereta` (
  `kode_kereta` varchar(5) NOT NULL,
  `nama_kereta` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_kota_asal`
--

CREATE TABLE `tb_travel_m_kota_asal` (
  `kode_asal` varchar(5) NOT NULL,
  `nama_kota_asal` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_kota_tujuan`
--

CREATE TABLE `tb_travel_m_kota_tujuan` (
  `kode_tujuan` varchar(5) NOT NULL,
  `nama_tujuan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_lapbeli_ka`
--

CREATE TABLE `tb_travel_m_lapbeli_ka` (
  `kode_lapbeli_tiket_ka` varchar(5) NOT NULL,
  `tanggal_lapbeli_tiket_ka` date NOT NULL,
  `validasi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_lapbeli_kl`
--

CREATE TABLE `tb_travel_m_lapbeli_kl` (
  `kode_lapbeli_tiket_kl` varchar(5) NOT NULL,
  `tanggal_lapbeli_tiket_kl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_lapbeli_pesawat`
--

CREATE TABLE `tb_travel_m_lapbeli_pesawat` (
  `kode_lapbeli_tiket_pesawat` varchar(5) NOT NULL,
  `tanggal_lapbeli_tiket_pesawat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_pesawat`
--

CREATE TABLE `tb_travel_m_pesawat` (
  `kode_pesawat` varchar(5) NOT NULL,
  `nama_pesawat` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_posisi`
--

CREATE TABLE `tb_travel_m_posisi` (
  `kode_posisi` varchar(2) NOT NULL,
  `nama_posisi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_sesi_ka`
--

CREATE TABLE `tb_travel_m_sesi_ka` (
  `kode_sesi` varchar(5) NOT NULL,
  `kode_keberangkatan` varchar(5) NOT NULL,
  `jam_berangkat` time NOT NULL,
  `jam_keluar` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_sesi_kl`
--

CREATE TABLE `tb_travel_m_sesi_kl` (
  `kode_sesi_kl` varchar(5) NOT NULL,
  `kode_keberangkatan_kl` varchar(5) NOT NULL,
  `jam_berangkat_kl` time NOT NULL,
  `jam_keluar_kl` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_sesi_pesawat`
--

CREATE TABLE `tb_travel_m_sesi_pesawat` (
  `kode_sesi_pesawat` varchar(5) NOT NULL,
  `kode_keberangkatan_pesawat` varchar(5) NOT NULL,
  `jam_berangkat` time NOT NULL,
  `jam_keluar` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_lapabsen`
--

CREATE TABLE `tb_travel_m_tgl_lapabsen` (
  `no_lap_absen` varchar(5) NOT NULL,
  `tgl_lap_absen` date NOT NULL,
  `validasi_lapabsen` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_lapkin`
--

CREATE TABLE `tb_travel_m_tgl_lapkin` (
  `no_lapkin` varchar(5) NOT NULL,
  `tgl_lapkin` date NOT NULL,
  `validasi_lapkin` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_lappengajuan`
--

CREATE TABLE `tb_travel_m_tgl_lappengajuan` (
  `no_pengajuan` varchar(5) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `validasi_pengajuan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_laprusak`
--

CREATE TABLE `tb_travel_m_tgl_laprusak` (
  `no_laprusak` varchar(5) NOT NULL,
  `tgl_laprusak` date NOT NULL,
  `validasi_laprusak` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_penambahan_kursi_ka`
--

CREATE TABLE `tb_travel_m_tgl_penambahan_kursi_ka` (
  `kode_penambahan_ka` varchar(5) NOT NULL,
  `tgl_penambahan_ka` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_penambahan_kursi_kl`
--

CREATE TABLE `tb_travel_m_tgl_penambahan_kursi_kl` (
  `kode_penambahan_kl` varchar(5) NOT NULL,
  `tgl_penambahan_kl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_m_tgl_penambahan_kursi_pesawat`
--

CREATE TABLE `tb_travel_m_tgl_penambahan_kursi_pesawat` (
  `no_penambahan_pesawat` varchar(5) NOT NULL,
  `tgl_penambahan_pesawat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_bangku_ka`
--

CREATE TABLE `tb_travel_r_bangku_ka` (
  `kode_bangku` varchar(5) NOT NULL,
  `kode_kereta` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_bangku_kl`
--

CREATE TABLE `tb_travel_r_bangku_kl` (
  `kode_bangku_kl` varchar(5) NOT NULL,
  `kode_kl` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_bangku_pesawat`
--

CREATE TABLE `tb_travel_r_bangku_pesawat` (
  `kode_bangku_pesawat` varchar(5) NOT NULL,
  `kode_pesawat` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lapabsen`
--

CREATE TABLE `tb_travel_r_lapabsen` (
  `no_lap_absen` varchar(5) NOT NULL,
  `NIP` varchar(7) NOT NULL,
  `jam_msk` time NOT NULL,
  `jam_keluar` time NOT NULL,
  `validasi_msk` varchar(15) NOT NULL,
  `validasi_keluar` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lapbeli_tiket_ka`
--

CREATE TABLE `tb_travel_r_lapbeli_tiket_ka` (
  `kode_lapbeli_tiket_ka` varchar(5) NOT NULL,
  `kode_kereta` varchar(5) NOT NULL,
  `kode_sesi` varchar(5) NOT NULL,
  `jumlah_yang_terjual` int(11) NOT NULL,
  `jumlah_Pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lapbeli_tiket_kl`
--

CREATE TABLE `tb_travel_r_lapbeli_tiket_kl` (
  `kode_lapbeli_tiket_kl` varchar(5) NOT NULL,
  `kode_kl` varchar(5) NOT NULL,
  `kode_sesi_kl` varchar(5) NOT NULL,
  `jumlah_yang_terjual` int(11) NOT NULL,
  `jumlah_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lapbeli_tiket_pesawat`
--

CREATE TABLE `tb_travel_r_lapbeli_tiket_pesawat` (
  `kode_lapbeli_tiket_pesawat` varchar(5) NOT NULL,
  `kode_pesawat` varchar(5) NOT NULL,
  `kode_sesi_pesawat` varchar(5) NOT NULL,
  `jumlah_yang_terjual` int(11) NOT NULL,
  `jumlah_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lapkin`
--

CREATE TABLE `tb_travel_r_lapkin` (
  `no_lapkin` varchar(5) NOT NULL,
  `kode_jabatan` varchar(2) NOT NULL,
  `NIP` varchar(7) NOT NULL,
  `tingkat_kinerja` enum('Baik','Sedang','Buruk','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_lappengajuan`
--

CREATE TABLE `tb_travel_r_lappengajuan` (
  `no_pengajuan` varchar(5) NOT NULL,
  `kode_kateg` varchar(5) NOT NULL,
  `alasan` varchar(25) NOT NULL,
  `estimasi_harga` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_laprusak`
--

CREATE TABLE `tb_travel_r_laprusak` (
  `no_laprusak` varchar(5) NOT NULL,
  `kode_aset` varchar(5) NOT NULL,
  `tgl_mulai_perbaiki` date NOT NULL,
  `tgl_selesai_perbaikan` date NOT NULL,
  `jenis_kerusakan` enum('Kecil','Sedang','Besar','Masive') NOT NULL,
  `status_sesudah` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_pembelian_tiket_ka`
--

CREATE TABLE `tb_travel_r_pembelian_tiket_ka` (
  `kode_pembelian` varchar(5) NOT NULL,
  `nama_penumpang` varchar(50) NOT NULL,
  `kode_kereta` varchar(5) NOT NULL,
  `kode_bangku` varchar(5) NOT NULL,
  `nomor_kursi` int(5) NOT NULL,
  `kode_keberangkatan` varchar(5) NOT NULL,
  `kode_sesi` varchar(5) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `harga` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_pembelian_tiket_kapal`
--

CREATE TABLE `tb_travel_r_pembelian_tiket_kapal` (
  `kode_pembelian` varchar(5) NOT NULL,
  `nama_penumpang` varchar(50) NOT NULL,
  `kode_kl` varchar(5) NOT NULL,
  `kode_bangku_kl` varchar(5) NOT NULL,
  `nomor_kursi` int(2) NOT NULL,
  `kode_keberangkatan_kl` varchar(5) NOT NULL,
  `kode_sesi_kl` varchar(5) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `harga` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_pembelian_tiket_pesawat`
--

CREATE TABLE `tb_travel_r_pembelian_tiket_pesawat` (
  `kode_pembelian` varchar(5) NOT NULL,
  `nama_penumpang` varchar(50) NOT NULL,
  `kode_pesawat` varchar(5) NOT NULL,
  `kode_bangku_pesawat` varchar(5) NOT NULL,
  `nomor_kursi` int(2) NOT NULL,
  `kode_keberangkatan_pesawat` varchar(5) NOT NULL,
  `kode_sesi_pesawat` varchar(5) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `harga` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_penambahan_kursi_ka`
--

CREATE TABLE `tb_travel_r_penambahan_kursi_ka` (
  `kode_penambahan_ka` varchar(5) NOT NULL,
  `kode_kereta` varchar(5) NOT NULL,
  `kode_sesi` varchar(5) NOT NULL,
  `kode_bangku` varchar(5) NOT NULL,
  `jumlah_bangku_input` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_penambahan_kursi_kl`
--

CREATE TABLE `tb_travel_r_penambahan_kursi_kl` (
  `kode_penambahan_kl` varchar(5) NOT NULL,
  `kode_kl` varchar(5) NOT NULL,
  `kode_sesi_kl` varchar(5) NOT NULL,
  `kode_bangku_kl` varchar(5) NOT NULL,
  `jumlah_bangku_input` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_travel_r_penambahan_kursi_pesawat`
--

CREATE TABLE `tb_travel_r_penambahan_kursi_pesawat` (
  `no_penambahan_pesawat` varchar(5) NOT NULL,
  `kode_pesawat` varchar(5) NOT NULL,
  `kode_sesi_pesawat` varchar(5) NOT NULL,
  `kode_bangku_pesawat` varchar(5) NOT NULL,
  `jumlah_bangku_input` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_travel_m_aset`
--
ALTER TABLE `tb_travel_m_aset`
  ADD PRIMARY KEY (`kode_aset`),
  ADD KEY `fk_kd_kateg` (`kode_kateg`);

--
-- Indexes for table `tb_travel_m_jabkaryawan`
--
ALTER TABLE `tb_travel_m_jabkaryawan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `tb_travel_m_jml_bangku_ka`
--
ALTER TABLE `tb_travel_m_jml_bangku_ka`
  ADD PRIMARY KEY (`kode_bangku`);

--
-- Indexes for table `tb_travel_m_jml_bangku_kl`
--
ALTER TABLE `tb_travel_m_jml_bangku_kl`
  ADD PRIMARY KEY (`kode_bangku_kl`);

--
-- Indexes for table `tb_travel_m_jml_bangku_pesawat`
--
ALTER TABLE `tb_travel_m_jml_bangku_pesawat`
  ADD PRIMARY KEY (`kode_bangku_pesawat`);

--
-- Indexes for table `tb_travel_m_kapal_laut`
--
ALTER TABLE `tb_travel_m_kapal_laut`
  ADD PRIMARY KEY (`kode_kl`);

--
-- Indexes for table `tb_travel_m_karyawan`
--
ALTER TABLE `tb_travel_m_karyawan`
  ADD PRIMARY KEY (`NIP`),
  ADD KEY `fk_kd_jab` (`kode_jabatan`),
  ADD KEY `fk_kd_posisi` (`kode_posisi`);

--
-- Indexes for table `tb_travel_m_kategpengajuan`
--
ALTER TABLE `tb_travel_m_kategpengajuan`
  ADD PRIMARY KEY (`kode_kateg`);

--
-- Indexes for table `tb_travel_m_keberangkatan_ka`
--
ALTER TABLE `tb_travel_m_keberangkatan_ka`
  ADD PRIMARY KEY (`kode_keberangkatan`),
  ADD KEY `fk_kd_asal` (`kode_asal`),
  ADD KEY `fk_kd_tujuan` (`kode_tujuan`);

--
-- Indexes for table `tb_travel_m_keberangkatan_kl`
--
ALTER TABLE `tb_travel_m_keberangkatan_kl`
  ADD PRIMARY KEY (`kode_keberangkatan_kl`),
  ADD KEY `fk_kd_asal_kl` (`kode_asal`),
  ADD KEY `fk_kd_tujuan_kl` (`kode_tujuan`);

--
-- Indexes for table `tb_travel_m_keberangkatan_pesawat`
--
ALTER TABLE `tb_travel_m_keberangkatan_pesawat`
  ADD PRIMARY KEY (`kode_keberangkatan_pesawat`),
  ADD KEY `fk_kd_asal_pesawat` (`kode_asal`),
  ADD KEY `fk_kd_tujuan_pesawat` (`kode_tujuan`);

--
-- Indexes for table `tb_travel_m_kereta`
--
ALTER TABLE `tb_travel_m_kereta`
  ADD PRIMARY KEY (`kode_kereta`);

--
-- Indexes for table `tb_travel_m_kota_asal`
--
ALTER TABLE `tb_travel_m_kota_asal`
  ADD PRIMARY KEY (`kode_asal`);

--
-- Indexes for table `tb_travel_m_kota_tujuan`
--
ALTER TABLE `tb_travel_m_kota_tujuan`
  ADD PRIMARY KEY (`kode_tujuan`);

--
-- Indexes for table `tb_travel_m_lapbeli_ka`
--
ALTER TABLE `tb_travel_m_lapbeli_ka`
  ADD PRIMARY KEY (`kode_lapbeli_tiket_ka`);

--
-- Indexes for table `tb_travel_m_lapbeli_kl`
--
ALTER TABLE `tb_travel_m_lapbeli_kl`
  ADD PRIMARY KEY (`kode_lapbeli_tiket_kl`);

--
-- Indexes for table `tb_travel_m_lapbeli_pesawat`
--
ALTER TABLE `tb_travel_m_lapbeli_pesawat`
  ADD PRIMARY KEY (`kode_lapbeli_tiket_pesawat`);

--
-- Indexes for table `tb_travel_m_pesawat`
--
ALTER TABLE `tb_travel_m_pesawat`
  ADD PRIMARY KEY (`kode_pesawat`);

--
-- Indexes for table `tb_travel_m_posisi`
--
ALTER TABLE `tb_travel_m_posisi`
  ADD PRIMARY KEY (`kode_posisi`);

--
-- Indexes for table `tb_travel_m_sesi_ka`
--
ALTER TABLE `tb_travel_m_sesi_ka`
  ADD PRIMARY KEY (`kode_sesi`),
  ADD KEY `fk_kd_keberangkatan` (`kode_keberangkatan`);

--
-- Indexes for table `tb_travel_m_sesi_kl`
--
ALTER TABLE `tb_travel_m_sesi_kl`
  ADD PRIMARY KEY (`kode_sesi_kl`),
  ADD KEY `fk_kd_keberangkatan_kl` (`kode_keberangkatan_kl`);

--
-- Indexes for table `tb_travel_m_sesi_pesawat`
--
ALTER TABLE `tb_travel_m_sesi_pesawat`
  ADD PRIMARY KEY (`kode_sesi_pesawat`),
  ADD KEY `fk_kd_keberangkatan_pesawat` (`kode_keberangkatan_pesawat`);

--
-- Indexes for table `tb_travel_m_tgl_lapabsen`
--
ALTER TABLE `tb_travel_m_tgl_lapabsen`
  ADD PRIMARY KEY (`no_lap_absen`);

--
-- Indexes for table `tb_travel_m_tgl_lapkin`
--
ALTER TABLE `tb_travel_m_tgl_lapkin`
  ADD PRIMARY KEY (`no_lapkin`);

--
-- Indexes for table `tb_travel_m_tgl_lappengajuan`
--
ALTER TABLE `tb_travel_m_tgl_lappengajuan`
  ADD PRIMARY KEY (`no_pengajuan`);

--
-- Indexes for table `tb_travel_m_tgl_laprusak`
--
ALTER TABLE `tb_travel_m_tgl_laprusak`
  ADD PRIMARY KEY (`no_laprusak`);

--
-- Indexes for table `tb_travel_m_tgl_penambahan_kursi_ka`
--
ALTER TABLE `tb_travel_m_tgl_penambahan_kursi_ka`
  ADD PRIMARY KEY (`kode_penambahan_ka`);

--
-- Indexes for table `tb_travel_m_tgl_penambahan_kursi_kl`
--
ALTER TABLE `tb_travel_m_tgl_penambahan_kursi_kl`
  ADD PRIMARY KEY (`kode_penambahan_kl`);

--
-- Indexes for table `tb_travel_m_tgl_penambahan_kursi_pesawat`
--
ALTER TABLE `tb_travel_m_tgl_penambahan_kursi_pesawat`
  ADD PRIMARY KEY (`no_penambahan_pesawat`);

--
-- Indexes for table `tb_travel_r_bangku_ka`
--
ALTER TABLE `tb_travel_r_bangku_ka`
  ADD KEY `fk_kd_bangku` (`kode_bangku`),
  ADD KEY `fk_kd_kereta` (`kode_kereta`);

--
-- Indexes for table `tb_travel_r_bangku_kl`
--
ALTER TABLE `tb_travel_r_bangku_kl`
  ADD KEY `fk_kd_bangku_kl` (`kode_bangku_kl`),
  ADD KEY `fk_kd_kl` (`kode_kl`);

--
-- Indexes for table `tb_travel_r_bangku_pesawat`
--
ALTER TABLE `tb_travel_r_bangku_pesawat`
  ADD KEY `fk_kd_bangku_pesawat` (`kode_bangku_pesawat`),
  ADD KEY `fk_kd_pesawat` (`kode_pesawat`);

--
-- Indexes for table `tb_travel_r_lapabsen`
--
ALTER TABLE `tb_travel_r_lapabsen`
  ADD KEY `fk_nip` (`NIP`),
  ADD KEY `fk_no_lap_absen` (`no_lap_absen`);

--
-- Indexes for table `tb_travel_r_lapbeli_tiket_ka`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_ka`
  ADD KEY `fk_kd_beli_ka` (`kode_lapbeli_tiket_ka`),
  ADD KEY `fk_kd_beli_kd_kereta` (`kode_kereta`),
  ADD KEY `fk_kd_beli_kd_sesi` (`kode_sesi`);

--
-- Indexes for table `tb_travel_r_lapbeli_tiket_kl`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_kl`
  ADD KEY `fk_kd_beli_kapallaut` (`kode_lapbeli_tiket_kl`),
  ADD KEY `fk_kd_beli_kl` (`kode_kl`),
  ADD KEY `fk_kd_sesi_kl` (`kode_sesi_kl`);

--
-- Indexes for table `tb_travel_r_lapbeli_tiket_pesawat`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_pesawat`
  ADD KEY `fk_kd_beli_pesawat` (`kode_lapbeli_tiket_pesawat`),
  ADD KEY `fk_kd_beli_kodepesawat` (`kode_pesawat`),
  ADD KEY `fk_kd_beli_kd_sesi_pesawat` (`kode_sesi_pesawat`);

--
-- Indexes for table `tb_travel_r_lapkin`
--
ALTER TABLE `tb_travel_r_lapkin`
  ADD KEY `fk_no_lapkin` (`no_lapkin`),
  ADD KEY `fk_kd_jab_2` (`kode_jabatan`),
  ADD KEY `fk_nip_2` (`NIP`);

--
-- Indexes for table `tb_travel_r_lappengajuan`
--
ALTER TABLE `tb_travel_r_lappengajuan`
  ADD KEY `fk_no_peng` (`no_pengajuan`),
  ADD KEY `fk_kode_kateg` (`kode_kateg`);

--
-- Indexes for table `tb_travel_r_laprusak`
--
ALTER TABLE `tb_travel_r_laprusak`
  ADD KEY `fk_no_lap` (`no_laprusak`),
  ADD KEY `fk_kode_aset` (`kode_aset`);

--
-- Indexes for table `tb_travel_r_pembelian_tiket_ka`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_ka`
  ADD PRIMARY KEY (`kode_pembelian`),
  ADD KEY `fk_kd_keretaa` (`kode_kereta`),
  ADD KEY `fk_kd_bangku1` (`kode_bangku`),
  ADD KEY `fk_kd_keberangkatan1` (`kode_keberangkatan`),
  ADD KEY `fk_kd_sesi1` (`kode_sesi`);

--
-- Indexes for table `tb_travel_r_pembelian_tiket_kapal`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_kapal`
  ADD PRIMARY KEY (`kode_pembelian`),
  ADD KEY `fk_kode_kl` (`kode_kl`),
  ADD KEY `fk_kode_keberangkatan_kl` (`kode_keberangkatan_kl`),
  ADD KEY `fk_kode_bangku_kl` (`kode_bangku_kl`),
  ADD KEY `fk_kode_sesi_kl` (`kode_sesi_kl`);

--
-- Indexes for table `tb_travel_r_pembelian_tiket_pesawat`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_pesawat`
  ADD PRIMARY KEY (`kode_pembelian`),
  ADD KEY `fk_kode_pesawat` (`kode_pesawat`),
  ADD KEY `fk_bangku_pesawat` (`kode_bangku_pesawat`),
  ADD KEY `fk_kode_keberangkatan_pesawat` (`kode_keberangkatan_pesawat`),
  ADD KEY `fk_kode_sesi_pesawat` (`kode_sesi_pesawat`);

--
-- Indexes for table `tb_travel_r_penambahan_kursi_ka`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_ka`
  ADD KEY `fk_kd_penambahan_ka` (`kode_penambahan_ka`),
  ADD KEY `fk_kd_penambahan_kd_ka` (`kode_kereta`),
  ADD KEY `fk_kd_penambahan_sesi_ka` (`kode_sesi`),
  ADD KEY `fk_kd_penambahan_bangku_ka` (`kode_bangku`);

--
-- Indexes for table `tb_travel_r_penambahan_kursi_kl`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_kl`
  ADD KEY `fk_kd_tambah_kl` (`kode_penambahan_kl`),
  ADD KEY `fk_kd_tambah_kd_kl` (`kode_kl`),
  ADD KEY `fk_kd_tambah_sesi_kl` (`kode_sesi_kl`),
  ADD KEY `fk_kd_tambah_bangku_kl` (`kode_bangku_kl`);

--
-- Indexes for table `tb_travel_r_penambahan_kursi_pesawat`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_pesawat`
  ADD KEY `fk_no_tambah_pesawat` (`no_penambahan_pesawat`),
  ADD KEY `fk_kd_pesawat_tambah` (`kode_pesawat`),
  ADD KEY `fk_kd_sesi_tambah` (`kode_sesi_pesawat`),
  ADD KEY `fk_kd_bangku_tambah` (`kode_bangku_pesawat`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_travel_m_aset`
--
ALTER TABLE `tb_travel_m_aset`
  ADD CONSTRAINT `fk_kd_kateg` FOREIGN KEY (`kode_kateg`) REFERENCES `tb_travel_m_kategpengajuan` (`kode_kateg`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_karyawan`
--
ALTER TABLE `tb_travel_m_karyawan`
  ADD CONSTRAINT `fk_kd_jab` FOREIGN KEY (`kode_jabatan`) REFERENCES `tb_travel_m_jabkaryawan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_posisi` FOREIGN KEY (`kode_posisi`) REFERENCES `tb_travel_m_posisi` (`kode_posisi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_keberangkatan_ka`
--
ALTER TABLE `tb_travel_m_keberangkatan_ka`
  ADD CONSTRAINT `fk_kd_asal` FOREIGN KEY (`kode_asal`) REFERENCES `tb_travel_m_kota_asal` (`kode_asal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tujuan` FOREIGN KEY (`kode_tujuan`) REFERENCES `tb_travel_m_kota_tujuan` (`kode_tujuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_keberangkatan_kl`
--
ALTER TABLE `tb_travel_m_keberangkatan_kl`
  ADD CONSTRAINT `fk_kd_asal_kl` FOREIGN KEY (`kode_asal`) REFERENCES `tb_travel_m_kota_asal` (`kode_asal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tujuan_kl` FOREIGN KEY (`kode_tujuan`) REFERENCES `tb_travel_m_kota_tujuan` (`kode_tujuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_keberangkatan_pesawat`
--
ALTER TABLE `tb_travel_m_keberangkatan_pesawat`
  ADD CONSTRAINT `fk_kd_asal_pesawat` FOREIGN KEY (`kode_asal`) REFERENCES `tb_travel_m_kota_asal` (`kode_asal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tujuan_pesawat` FOREIGN KEY (`kode_tujuan`) REFERENCES `tb_travel_m_kota_tujuan` (`kode_tujuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_sesi_ka`
--
ALTER TABLE `tb_travel_m_sesi_ka`
  ADD CONSTRAINT `fk_kd_keberangkatan` FOREIGN KEY (`kode_keberangkatan`) REFERENCES `tb_travel_m_keberangkatan_ka` (`kode_keberangkatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_sesi_kl`
--
ALTER TABLE `tb_travel_m_sesi_kl`
  ADD CONSTRAINT `fk_kd_keberangkatan_kl` FOREIGN KEY (`kode_keberangkatan_kl`) REFERENCES `tb_travel_m_keberangkatan_kl` (`kode_keberangkatan_kl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_m_sesi_pesawat`
--
ALTER TABLE `tb_travel_m_sesi_pesawat`
  ADD CONSTRAINT `fk_kd_keberangkatan_pesawat` FOREIGN KEY (`kode_keberangkatan_pesawat`) REFERENCES `tb_travel_m_keberangkatan_pesawat` (`kode_keberangkatan_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_bangku_ka`
--
ALTER TABLE `tb_travel_r_bangku_ka`
  ADD CONSTRAINT `fk_kd_bangku` FOREIGN KEY (`kode_bangku`) REFERENCES `tb_travel_m_jml_bangku_ka` (`kode_bangku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_kereta` FOREIGN KEY (`kode_kereta`) REFERENCES `tb_travel_m_kereta` (`kode_kereta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_bangku_kl`
--
ALTER TABLE `tb_travel_r_bangku_kl`
  ADD CONSTRAINT `fk_kd_bangku_kl` FOREIGN KEY (`kode_bangku_kl`) REFERENCES `tb_travel_m_jml_bangku_kl` (`kode_bangku_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_kl` FOREIGN KEY (`kode_kl`) REFERENCES `tb_travel_m_kapal_laut` (`kode_kl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_bangku_pesawat`
--
ALTER TABLE `tb_travel_r_bangku_pesawat`
  ADD CONSTRAINT `fk_kd_bangku_pesawat` FOREIGN KEY (`kode_bangku_pesawat`) REFERENCES `tb_travel_m_jml_bangku_pesawat` (`kode_bangku_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_pesawat` FOREIGN KEY (`kode_pesawat`) REFERENCES `tb_travel_m_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lapabsen`
--
ALTER TABLE `tb_travel_r_lapabsen`
  ADD CONSTRAINT `fk_nip` FOREIGN KEY (`NIP`) REFERENCES `tb_travel_m_karyawan` (`NIP`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_lap_absen` FOREIGN KEY (`no_lap_absen`) REFERENCES `tb_travel_m_tgl_lapabsen` (`no_lap_absen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lapbeli_tiket_ka`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_ka`
  ADD CONSTRAINT `fk_kd_beli_ka` FOREIGN KEY (`kode_lapbeli_tiket_ka`) REFERENCES `tb_travel_m_lapbeli_ka` (`kode_lapbeli_tiket_ka`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_beli_kd_kereta` FOREIGN KEY (`kode_kereta`) REFERENCES `tb_travel_m_kereta` (`kode_kereta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_beli_kd_sesi` FOREIGN KEY (`kode_sesi`) REFERENCES `tb_travel_m_sesi_ka` (`kode_sesi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lapbeli_tiket_kl`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_kl`
  ADD CONSTRAINT `fk_kd_beli_kapallaut` FOREIGN KEY (`kode_lapbeli_tiket_kl`) REFERENCES `tb_travel_m_lapbeli_kl` (`kode_lapbeli_tiket_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_beli_kl` FOREIGN KEY (`kode_kl`) REFERENCES `tb_travel_m_kapal_laut` (`kode_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_sesi_kl` FOREIGN KEY (`kode_sesi_kl`) REFERENCES `tb_travel_m_sesi_kl` (`kode_sesi_kl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lapbeli_tiket_pesawat`
--
ALTER TABLE `tb_travel_r_lapbeli_tiket_pesawat`
  ADD CONSTRAINT `fk_kd_beli_kd_sesi_pesawat` FOREIGN KEY (`kode_sesi_pesawat`) REFERENCES `tb_travel_m_sesi_pesawat` (`kode_sesi_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_beli_kodepesawat` FOREIGN KEY (`kode_pesawat`) REFERENCES `tb_travel_m_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_beli_pesawat` FOREIGN KEY (`kode_lapbeli_tiket_pesawat`) REFERENCES `tb_travel_m_lapbeli_pesawat` (`kode_lapbeli_tiket_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lapkin`
--
ALTER TABLE `tb_travel_r_lapkin`
  ADD CONSTRAINT `fk_kd_jab_2` FOREIGN KEY (`kode_jabatan`) REFERENCES `tb_travel_m_jabkaryawan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_nip_2` FOREIGN KEY (`NIP`) REFERENCES `tb_travel_m_karyawan` (`NIP`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_lapkin` FOREIGN KEY (`no_lapkin`) REFERENCES `tb_travel_m_tgl_lapkin` (`no_lapkin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_lappengajuan`
--
ALTER TABLE `tb_travel_r_lappengajuan`
  ADD CONSTRAINT `fk_kode_kateg` FOREIGN KEY (`kode_kateg`) REFERENCES `tb_travel_m_kategpengajuan` (`kode_kateg`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_peng` FOREIGN KEY (`no_pengajuan`) REFERENCES `tb_travel_m_tgl_lappengajuan` (`no_pengajuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_laprusak`
--
ALTER TABLE `tb_travel_r_laprusak`
  ADD CONSTRAINT `fk_kode_aset` FOREIGN KEY (`kode_aset`) REFERENCES `tb_travel_m_aset` (`kode_aset`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_lap` FOREIGN KEY (`no_laprusak`) REFERENCES `tb_travel_m_tgl_laprusak` (`no_laprusak`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_pembelian_tiket_ka`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_ka`
  ADD CONSTRAINT `fk_kd_bangku1` FOREIGN KEY (`kode_bangku`) REFERENCES `tb_travel_m_jml_bangku_ka` (`kode_bangku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_keberangkatan1` FOREIGN KEY (`kode_keberangkatan`) REFERENCES `tb_travel_m_keberangkatan_ka` (`kode_keberangkatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_keretaa` FOREIGN KEY (`kode_kereta`) REFERENCES `tb_travel_m_kereta` (`kode_kereta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_sesi1` FOREIGN KEY (`kode_sesi`) REFERENCES `tb_travel_m_sesi_ka` (`kode_sesi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_pembelian_tiket_kapal`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_kapal`
  ADD CONSTRAINT `fk_kode_bangku_kl` FOREIGN KEY (`kode_bangku_kl`) REFERENCES `tb_travel_m_jml_bangku_kl` (`kode_bangku_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_keberangkatan_kl` FOREIGN KEY (`kode_keberangkatan_kl`) REFERENCES `tb_travel_m_keberangkatan_kl` (`kode_keberangkatan_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_kl` FOREIGN KEY (`kode_kl`) REFERENCES `tb_travel_m_kapal_laut` (`kode_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_sesi_kl` FOREIGN KEY (`kode_sesi_kl`) REFERENCES `tb_travel_m_sesi_kl` (`kode_sesi_kl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_pembelian_tiket_pesawat`
--
ALTER TABLE `tb_travel_r_pembelian_tiket_pesawat`
  ADD CONSTRAINT `fk_bangku_pesawat` FOREIGN KEY (`kode_bangku_pesawat`) REFERENCES `tb_travel_m_jml_bangku_pesawat` (`kode_bangku_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_keberangkatan_pesawat` FOREIGN KEY (`kode_keberangkatan_pesawat`) REFERENCES `tb_travel_m_keberangkatan_pesawat` (`kode_keberangkatan_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_pesawat` FOREIGN KEY (`kode_pesawat`) REFERENCES `tb_travel_m_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kode_sesi_pesawat` FOREIGN KEY (`kode_sesi_pesawat`) REFERENCES `tb_travel_m_sesi_pesawat` (`kode_sesi_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_penambahan_kursi_ka`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_ka`
  ADD CONSTRAINT `fk_kd_penambahan_bangku_ka` FOREIGN KEY (`kode_bangku`) REFERENCES `tb_travel_m_jml_bangku_ka` (`kode_bangku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_penambahan_ka` FOREIGN KEY (`kode_penambahan_ka`) REFERENCES `tb_travel_m_tgl_penambahan_kursi_ka` (`kode_penambahan_ka`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_penambahan_kd_ka` FOREIGN KEY (`kode_kereta`) REFERENCES `tb_travel_m_kereta` (`kode_kereta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_penambahan_sesi_ka` FOREIGN KEY (`kode_sesi`) REFERENCES `tb_travel_m_sesi_ka` (`kode_sesi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_penambahan_kursi_kl`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_kl`
  ADD CONSTRAINT `fk_kd_tambah_bangku_kl` FOREIGN KEY (`kode_bangku_kl`) REFERENCES `tb_travel_m_jml_bangku_kl` (`kode_bangku_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tambah_kd_kl` FOREIGN KEY (`kode_kl`) REFERENCES `tb_travel_m_kapal_laut` (`kode_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tambah_kl` FOREIGN KEY (`kode_penambahan_kl`) REFERENCES `tb_travel_m_tgl_penambahan_kursi_kl` (`kode_penambahan_kl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_tambah_sesi_kl` FOREIGN KEY (`kode_sesi_kl`) REFERENCES `tb_travel_m_sesi_kl` (`kode_sesi_kl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_travel_r_penambahan_kursi_pesawat`
--
ALTER TABLE `tb_travel_r_penambahan_kursi_pesawat`
  ADD CONSTRAINT `fk_kd_bangku_tambah` FOREIGN KEY (`kode_bangku_pesawat`) REFERENCES `tb_travel_m_jml_bangku_pesawat` (`kode_bangku_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_pesawat_tambah` FOREIGN KEY (`kode_pesawat`) REFERENCES `tb_travel_m_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kd_sesi_tambah` FOREIGN KEY (`kode_sesi_pesawat`) REFERENCES `tb_travel_m_sesi_pesawat` (`kode_sesi_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_tambah_pesawat` FOREIGN KEY (`no_penambahan_pesawat`) REFERENCES `tb_travel_m_tgl_penambahan_kursi_pesawat` (`no_penambahan_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
